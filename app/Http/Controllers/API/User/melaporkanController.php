<?php

namespace App\Http\Controllers\API\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class melaporkanController extends Controller
{
    public function melaporkan(Request $request)
    {
        $result = $this->myClient->post($this->apiURL.'melaporkan',[
            'headers' => [
                'X-CSRF-TOKEN' => $request->_token
            ],
            'multipart' => [
                [
                    'name' => 'lapor_id',
                    'contents' => $request->lapor_id
                ],
                [
                    'name' => 'nama',
                    'contents' => $request->nama
                ],
                [
                    'name' => 'jeniskelamin',
                    'contents' => $request->jeniskelamin
                ],
                [
                    'name' => 'email',
                    'contents' => $request->email
                ],
                [
                    'name' => 'nohp',
                    'contents' => $request->nohp
                ],
                [
                    'name' => 'kota',
                    'contents' => $request->kota
                ],
                [
                    'name' => 'bukti',
                    'contents' => fopen($request->bukti, 'r'),
                    'filename' => $request->bukti->getClientOriginalName()
                ]
            ]
        ]);
        $response = $result->getBody();
        $show = json_decode($response,true);
        // return dd($show);
        return redirect('/')->with('success','Terimakasih sudah melaporkan');
    }
}