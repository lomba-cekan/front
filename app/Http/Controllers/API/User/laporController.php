<?php

namespace App\Http\Controllers\API\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\MultipartStream;

class laporController extends Controller
{
    public function homeLapor(){
        $result = $this->myClient->get($this->apiURL.'last',[
            'headers' => [
            'Accept' => 'application/json'
            ]
        ]);
        $response = $result->getBody();
        $show = json_decode($response,true);
        // session()->put('UserID',$show);
        return view('pages.home.index',['show'=>$show]);
        // return dd($show);
    }
    public function frontLapor(){
        return view('pages.laporkehilangan.index');
    }
    public function addLapor(Request $request){
        $result = $this->myClient->post($this->apiURL.'lapor',[
            'headers' => [
                'X-CSRF-TOKEN' => $request->_token
            ],
            'multipart' => [
                [
                    'name' => 'nama',
                    'contents' => $request->nama
                ],
                [
                    'name' => 'jeniskelamin',
                    'contents' => $request->jeniskelamin
                ],
                [
                    'name' => 'email',
                    'contents' => $request->email
                ],
                [
                    'name' => 'nohp',
                    'contents' => $request->nohp
                ],
                [
                    'name' => 'platno',
                    'contents' => $request->platno
                ],
                [
                    'name' => 'norangka',
                    'contents' => $request->norangka
                ],
                [
                    'name' => 'jeniskendaraan',
                    'contents' => $request->jeniskendaraan
                ],
                [
                    'name' => 'kota',
                    'contents' => $request->kota
                ],
                [
                    'name' => 'keterangantambahan',
                    'contents' => $request->keterangantambahan
                ],
                [
                    'name' => 'peristiwa',
                    'contents' => $request->peristiwa
                ],
                [
                    'name' => 'bukti',
                    'contents' => fopen($request->bukti, 'r'),
                    'filename' => $request->bukti->getClientOriginalName()
                ]
            ]
        ]);
        $response = $result->getBody();
        $show = json_decode($response,true);
        // return dd($show);
        return redirect('/lapor')->with('success','Berhasil');
    }
    public function showIdLapor(Request $request){
        $result = $this->myClient->get($this->apiURL.'lapor/'.$request->id,[
            'headers' => [
            'Accept' => 'application/json'
            ]
        ]);
        $response = $result->getBody();
        $show = json_decode($response,true);
        // session()->put('UserID',$show);
        // return view('pages.lihat.lihat',['show'=>$show]);
        return dd($show);
    }
    public function searchLapor(Request $request){
        $result = $this->myClient->post($this->apiURL.'cari',[
            'form_params' => [
                'cari' => $request->cari
            ]
        ]);
        $response = $result->getBody();
        $show = json_decode($response,true);
         return view('pages.cari.index',['show'=>$show]);
    }
}
