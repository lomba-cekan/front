<?php

namespace App\Http\Controllers\API\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\ImageManagerStatic as Image;

class laporController extends Controller
{
    public function home(){
        $result = $this->myClient->get($this->apiURL.'admin/lapor',[
            'headers' => [
            'Accept' => 'application/json'
            ]
        ]);
        $response = $result->getBody();
        $show = json_decode($response,true);
        return view('admin.pages.lapor.index',['show'=>$show]);
    }
    public function login(){
        return view('admin.auth.index');
    }
    public function lihat(Request $request){
        $result = $this->myClient->get($this->apiURL.'admin/lapor/'.$request->id,[
            'headers' => [
            'Accept' => 'application/json'
            ]
        ]);
        $response = $result->getBody();
        $show = json_decode($response,true);
        $img = Image::make($this->imageURL.'upld/imgs/vrf/'.$show['bukti']);
        $img->encode('png');
        $type = 'png';
        $base64 = 'data:image/'.$type.';base64,'.base64_encode($img);
        $show['bukti'] = $base64;
        // return dd($img);
        return view('admin.pages.lihat.index',['show'=>$show]);
    }
    public function Verification(Request $request){
        $result = $this->myClient->put($this->apiURL.'admin/lapor/'.$request->lapor_id,[
            'headers' => [
                'X-CSRF-TOKEN' => $request->_token,
                'Content-Type' => 'application/json'
            ],
            'json' => [
                'lapor_id' => $request->lapor_id
            ]
        ]);
        $response = $result->getBody();
        $show = json_decode($response,true);
        return dd($show);
    }
    public function lihatPelapor(Request $request){
        $result = $this->myClient->get($this->apiURL.'admin/pelapor/'.$request->id,[
            'headers' => [
            'Accept' => 'application/json'
            ]
        ]);
        $response = $result->getBody();
        $show = json_decode($response,true);
        $img = Image::make($this->imageURL.'upld/imgs/mlprkan/'.$show['bukti']);
        $img->encode('png');
        $type = 'png';
        $base64 = 'data:image/'.$type.';base64,'.base64_encode($img);
        $show['bukti'] = $base64;
        // return dd($img);
        return view('admin.pages.tm.index',['show'=>$show]);
    }
}
