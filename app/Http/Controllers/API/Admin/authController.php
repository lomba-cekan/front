<?php

namespace App\Http\Controllers\API\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class authController extends Controller
{
    public function login(Request $request){
        $result = $this->myClient->post($this->apiURL.'auth/login',[
            'form_params' => [
                'email' => $request->email,
                'password' => $request->password
            ]
        ]);
        $response = $result->getBody();
        $token = json_decode($response,true);
        session()->put('token',$token);
        // return $show;
        return redirect('/adnmst4tor/home');
    }
    public function logout(){
        session()->forget('token');
        return redirect('/adnmst4tor');
    }
}
