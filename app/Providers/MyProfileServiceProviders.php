<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\MultipartStream;

class MyProfileServiceProviders extends ServiceProvider
{
    /*
    * get User ID.
    *
    * @return json
    */
    public function getUser(){
        $result = $this->myClient->get($this->apiURL.'myuser',[
            'headers' => [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer'.$request->session()->get('token')->token
            ]
        ]);
        $response = $result->getBody();
        $show = json_decode($response,true);
        return true;
    }

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $result = $this->myClient->get($this->apiURL.'myuser',[
            'headers' => [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer'.$request->session()->get('token')->token
            ]
        ]);
        $response = $result->getBody();
        $show = json_decode($response,true);
        view()->composer('admin.layouts.app', function($data){
            $data->with('data', array('test' => 'test'));
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
