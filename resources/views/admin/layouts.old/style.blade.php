    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="/assets/global/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="/assets/global/css/dashboard.css" rel="stylesheet" type="text/css">
    <link href="/assets/global/css/ie10-viewport-bug-workaround.css" rel="stylesheet" type="text/css">
    <link href="/assets/global/css/dataTables.bootstrap.min.css" media="all" rel="stylesheet" type="text/css" />
    <style>
    .footer {
       position:fixed;
       padding: 0 10px 0 10px;                 
       left: 0px;
       bottom: 0px;
       width: 100%;
    }
    </style>