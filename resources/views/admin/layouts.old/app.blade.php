<!DOCTYPE html>
@if(session('!token'))
    <script type="text/javascript">
        window.location = "/adnmst4tor/";//here double curly bracket
    </script>
@endif
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Cekan - Administrator</title>

@include('admin.layouts.style')

@yield('css')

</head>
<body>
@include('admin.layouts.header')

@yield('content')
        
@include('admin.layouts.footer')

    <!-- javascript -->
@include('admin.layouts.js')
@yield('js')

</body>
</html>
