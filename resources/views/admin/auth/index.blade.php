@extends('layouts.app')
@section('header')
        {{-- <a href="/">Home</a> --}}
@endsection
@section('css')
    @include('admin.auth.css')
@endsection

@section('content')

@if(session('token'))
    <script type="text/javascript">
        window.location = "/adnmst4tor/home";//here double curly bracket
    </script>
@endif
<div class="container">    
<div id="signupbox" class="rafi">
    <div class="panel panel-info">
        <div class="panel-heading">
            <div class="panel-title">Masuk Halaman Administrator</div>
            <div class="rafh"><a id="signinlink" href="#"></a>
            </div>
        </div>  
        <div class="panel-body" >
        @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            <form id="signupform" class="form-horizontal" method="POST" action="/auth/login" role="form">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="email" class="col-md-3 control-label">Email</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" name="email" placeholder="Email Address">
                </div>
            </div>
            <div class="form-group">
                <label for="password" class="col-md-3 control-label">Password</label>
                <div class="col-md-9">
                    <input type="password" class="form-control" name="password" placeholder="Enter Password">
                </div>
            </div>
            <div class="form-group">
                <!-- Button -->                                        
                <div class="col-md-offset-3 col-md-9">
                    <button id="btn-signup" type="submit" class="btn btn-info btn-block"><i class="icon-hand-right"></i> &nbsp Masuk</button>  
            </div>
        </form>
        </div>
        </div>
        </div>
    </div>
</div>
@endsection
@section('js')
    @include('admin.auth.js')
@endsection