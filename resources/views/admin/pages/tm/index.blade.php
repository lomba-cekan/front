@extends('admin.layouts.app')

@section('css')
    @include('admin.pages.lihat.css')
@endsection

@section('content')
<!-- Page Content -->
    <div class="container">

        <!-- Portfolio Item Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{ $show['laporkehilangan']['jeniskendaraan'] }}
                    <small>{{ $show['laporkehilangan']['norangka'] }} # {{ $show['id'] }}</small>
                </h1>
            </div>
        </div>
        <!-- /.row -->

        <!-- Portfolio Item Row -->
        <div class="row">
            <div class="col-md-8">
                <img class="img-responsive" src="{{ $show['bukti'] }}" alt="">
            </div>

            <div class="col-md-4">
                <h3>Kehilangan # {{ $show['laporkehilangan']['platno'] }}</h3>
                <p><b>Peristiwa: </b><br>{{ $show['laporkehilangan']['peristiwa'] }}</p>
                <h3>Info Pelapor : </h3>
                <ul align="left">
                    <b>Nama:</b> {{ $show['nama'] }}<br>
                    <b>No HP:</b> {{ $show['nohp'] }}<br>
                    <b>Email:</b> {{ $show['email'] }}<br>
                </ul>
            </div>
        </div>
        <!-- /.row -->

        </div>
        <!-- /.row -->

        <hr>


@endsection