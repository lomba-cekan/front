@if($data['verifikasi'] == '1')
    <a href="lihat/{{ $data['id'] }}" type="button" class="btn btn-xs btn-danger">Batalkan Verifikasi</a>
@endif
@if($data['verifikasi'] == '0')
    <a href="lihat/{{ $data['id'] }}" type="button" class="btn btn-xs btn-warning">Verifikasi Sekarang</a>
@endif