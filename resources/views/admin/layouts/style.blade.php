    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <!--<link href="/assets/global/css/bootstrap.css" rel="stylesheet" type="text/css">-->
    <!--<link href="/assets/global/css/dashboard.css" rel="stylesheet" type="text/css">-->
    <!--<link href="/assets/global/css/ie10-viewport-bug-workaround.css" rel="stylesheet" type="text/css">-->
    <!-- Bootstrap -->
    <link href="/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="/vendors/iCheck/skins/flat/green.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="/build/css/custom.min.css" rel="stylesheet">
    <style>
    .footer {
       position:fixed;
       padding: 0 10px 0 10px;                 
       left: 0px;
       bottom: 0px;
       width: 100%;
    }
    </style>