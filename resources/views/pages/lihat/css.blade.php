<link href="/assets/global/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
<style>
@import url('https://fonts.googleapis.com/css?family=Roboto');
body {
  background-image: url(/assets/global/img/bg2.jpg);
  background-position: center center;
  background-repeat: no-repeat;
  background-attachment: fixed;
  background-size: cover;
  background-color:#464646;
}

/* For mobile devices */
@media only screen and (max-width: 767px) {
body {
    background-image: url(/assets/global/img/bg2.jpg);
}
}
.rafi {
    margin-top: 0 auto;
    margin-bottom: 3%;
    height: 150%;
}
.rafh {
    font-size: 85%;
}
label{
    color:#999;  
    padding-top: 5px; 
    display:block; 
    left:0;
}

.panel{
    border-width:2px; 
    border-radius:7px;
    width: 50%;
    opacity: 0.8;
    position: relative;
    margin: 0 auto;
    text-align: left;
    margin-top: 15%;
    margin-bottom: 3%;
    height: 150%;
    }
.panel-utama > .panel-kepala{
    background-color: #9d6617;
    border-color: #9d6617;
}
.table-bordered > thead > tr > th{
    color: #f6003d;
}

.table-bordered > thead > tr > th,
.table-bordered > tbody > tr > th,
.table-bordered > tfoot > tr > th,
.table-bordered > thead > tr > td,
.table-bordered > tbody > tr > td,
.table-bordered > tfoot > tr > td{
    border:2px solid #eee; 
    vertical-align: middle;
    text-align: left;
       padding: 10px 10px 10px 70px; 
}
.table-bordered{
    border-radius:7px;
}
</style>