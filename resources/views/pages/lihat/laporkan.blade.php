<!-- Modal -->
<div class="modal fade" id="melaporkan" tabindex="-1" role="dialog" aria-labelledby="melaporkan" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="melaporkan">Melaporkan Plat Nomer # {{ $show['platno'] }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form id="signupform" class="form-horizontal" method="POST" enctype="multipart/form-data" action="/melaporkan/submit" role="form">
            {{ csrf_field() }}
            <input type="hidden" name="lapor_id" value="{{ $show['id'] }}">
            <div class="form-group">
                <label for="nama" class="col-md-3 control-label">Nama Lengkap</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" name="nama" placeholder="Nama Lengkap Anda">
                </div>
            </div>
            <div class="form-group">
                <label for="jeniskelamin" class="col-md-3 control-label">Jenis Kelamin</label>
                <div class="col-md-3">
                    <label class="radio"><input type="radio" name="jeniskelamin" value="1">Laki Laki</label>
                    <label class="radio"><input type="radio" name="jeniskelamin" value="2">Perempuan</label>
                </div>
            </div>
            <div class="form-group">
                    <label for="email" class="col-md-3 control-label">Alamat Email</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="email" placeholder="Masukan alamat Email anda">
                    </div>
                </div>        
            <div class="form-group">
                    <label for="nohp" class="col-md-3 control-label">Nomer Handphone</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="nohp" placeholder="Masukan nomer Handphone anda">
                    </div>
                </div>
            <div class="form-group">
                <label for="kota" class="col-md-3 control-label">Ditemukan di Kota</label>
                <div class="col-md-9">
                    <select class="form-control" id="kota" name="kota">
                    @include('pages.laporkehilangan.kota')
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Foto bukti melihatnyaS</label>
                <div class="col-md-9">
                <input id="input-1" type="file" class="file" name="bukti" placeholder="Masukan Foto bukti ">
                </div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
        <button type="submit" class="btn btn-primary">Kirim</button>
      </div>
      </form>
    </div>
  </div>
</div>