@extends('layouts.app')
@section('header')
        <a href="/">Cari Kendaraan</a>
@endsection
@section('css')
    @include('pages.lihat.css')
@endsection

@section('content')
 <div class="panel panel-primary">
              <div class="panel-heading" style="background-color: #9d6617;">Informasi Kendaraan Hilang # 
                    <a type="button" class="btn btn-xs btn-danger">Kehilangan</a></div>
              <table class="table table-bordered" style="color: #9d6617;">
               
                <tbody>
                  <tr class="danger">
                    <td>
                    @foreach($show as $show)
                    Nama: {{ $show['nama'] }}<br>
                    Email: {{ $show['email'] }}<br>
                    Plat Nomer: {{ $show['platno'] }}<br>
                    Kehilangan di Kota: {{ $show['kota'] }}<br>
                    Jenis Kendaraan: {{ $show['jeniskendaraan']}}<br>
                    Peristiwa: {{ $show['peristiwa'] }}<br>
                    Keterangan Tambahan {{ $show['keterangantambahan'] }}
                    @break;
                    @endforeach
                    <br>
                    </td>
                    <td>
                    <!-- Button trigger modal -->
                  <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#melaporkan">
                    Melihatnya? Laporkan!
                  </button>
                  </td>
                  </tr>
                </tbody>
              </table>
            </div>
    </div>
</div>
@include('pages.lihat.laporkan')

@endsection
@section('js')
    @include('pages.lihat.js')
@endsection