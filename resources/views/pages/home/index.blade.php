@extends('layouts.app')
@section('content')
<div class="flex-center position-ref full-height">
    <div class="content">
    <img class="logo" src="/assets/global/img/logo.png"></img>
    <form action="/cari" method="post">
      {{ csrf_field() }}
        <div id="custom-search-input">
            <div class="input-group col-md-12">
                <input type="text" class="search-query form-control" name="cari" placeholder="Search" />
                <span class="input-group-btn">
                <button class="btn btn-danger" type="button">
                <span class="glyphicon glyphicon-search"></span>
                </button>
                </span>
            </div>
        </div>
    </form>
    <div class="panel panel-primary">
              <div class="panel-heading" style="background-color: #9d6617;">Terakhir Kehilangan</div>
              <table class="table table-bordered" style="color: #9d6617;">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Nama</th>
                    <th>Plat Nomer</th>
                  </tr>
                </thead>
                <tbody>
                @foreach($show as $data)
                  <tr class="danger">
                    <td><a href="lihat/{{ $data['id'] }}">{{ $data['id'] }}</a></td>
                    <td>{{ $data['nama'] }}</td>
                    <td>{{ $data['platno'] }} </td>
                  </tr>
                @endforeach
                </tbody>
              </table>
            </div>
    </div>
</div>
@endsection
