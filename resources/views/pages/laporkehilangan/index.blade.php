@extends('layouts.app')
@section('header')
        <a href="/">Cari Kendaraan</a>
@endsection
@section('css')
    @include('pages.laporkehilangan.css')
@endsection

@section('content')
<div class="container">    
<div id="signupbox" class="rafi">
    <div class="panel panel-info">
        <div class="panel-heading">
            <div class="panel-title">Melapor</div>
            <div class="rafh"><a id="signinlink" href="#"></a>
            </div>
        </div>  
        <div class="panel-body" >
        @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            <form id="signupform" class="form-horizontal" method="POST" enctype="multipart/form-data" action="/lapor/submit" role="form">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="nama" class="col-md-3 control-label">Nama Lengkap</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" name="nama" placeholder="Nama Lengkap">
                </div>
            </div>
            <div class="form-group">
                <label for="jeniskelamin" class="col-md-3 control-label">Jenis Kelamin</label>
                <div class="col-md-3">
                    <label class="radio-inline"><input type="radio" name="jeniskelamin" value="1">Laki Laki</label>
                    <label class="radio-inline"><input type="radio" name="jeniskelamin" value="2">Perempuan</label>
                </div>
            </div>
            <div class="form-group">
                    <label for="email" class="col-md-3 control-label">Alamat Email</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="email" placeholder="Masukan alamat Email">
                    </div>
                </div>        
            <div class="form-group">
                    <label for="nohp" class="col-md-3 control-label">Nomer Handphone</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="nohp" placeholder="Masukan nomer Handphone">
                    </div>
                </div>
            <div class="form-group">
                <label for="platno" class="col-md-3 control-label">Plat Nomer Kendaraan</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" name="platno" placeholder="Masukan Plat Nomer Kendaraan Yang Hilang">
                </div>
            </div>
            <div class="form-group">
                <label for="platno" class="col-md-3 control-label">No Rangka Kendaraan</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" name="norangka" placeholder="Masukan Nomer Rangka Kendaraan Yang Hilang">
                </div>
            </div>
            <div class="form-group">
                <label for="platno" class="col-md-3 control-label">Jenis Kendaraan Kendaraan</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" name="jeniskendaraan" placeholder="Masukan Jenis Kendaraan Kendaraan Yang Hilang">
                </div>
            </div>
            <div class="form-group">
                <label for="kota" class="col-md-3 control-label">Kehilangan di Kota</label>
                <div class="col-md-9">
                    <select class="form-control" id="kota" name="kota">
                    @include('pages.laporkehilangan.kota')
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="keterangantambahan" class="col-md-3 control-label">Keterangan Tambahan</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" name="keterangantambahan" placeholder="Masukan Keterangan Kendaraan Yang Hilang">
                </div>
            </div>
            <div class="form-group">
                <label for="peristiwa" class="col-md-3 control-label">Peristiwa Kejadian</label>
                <div class="col-md-9">
                    <textarea class="form-control" name="peristiwa" placeholder="Bagaimana Kejadian Peristiwa Tersebut?"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Foto Verifikasi Kehilangan( dari Kepolisian )</label>
                <div class="col-md-9">
                <input id="input-1" type="file" class="file" name="bukti" placeholder="Masukan Foto bukti tanda kehilangan">
                </div>
            </div>
            <div class="form-group">
                <!-- Button -->                                        
                <div class="col-md-offset-3 col-md-9">
                    <button id="btn-signup" type="submit" class="btn btn-info btn-block"><i class="icon-hand-right"></i> &nbsp Kirim</button>  
            </div>
        </form>
        </div>
        </div>
        </div>
    </div>
</div>
@endsection
@section('js')
    @include('pages.laporkehilangan.js')
@endsection