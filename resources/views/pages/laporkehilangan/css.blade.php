<link href="/assets/global/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
<style>
@import url('https://fonts.googleapis.com/css?family=Roboto');
body {
  background-image: url(/assets/global/img/bg2.jpg);
  background-position: center center;
  background-repeat: no-repeat;
  background-attachment: fixed;
  background-size: cover;
  background-color:#464646;
}

/* For mobile devices */
@media only screen and (max-width: 767px) {
body {
    background-image: url(/assets/global/img/bg2.jpg);
}
}
.rafi {
    margin-top: 4%;
    margin-bottom: 3%;
    height: 150%;
}
.rafh {
    font-size: 85%;
}
.panel {
    opacity: 0.9;
    width: 80%;
}
.panel-info > .panel-heading {
    background-color: #fff;
    color: black;
    font-family: 'Roboto', sans-serif;
}
</style>