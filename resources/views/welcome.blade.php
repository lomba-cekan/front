<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="/css/style.css" rel="stylesheet" type="text/css">
    <link href="/assets/global/css/bootstrap.css" rel="stylesheet" type="text/css">
</head>
<body>
<nav class="navbar bg-faded">
    <div class="r-right">
      <a href="/blog">Blog</a>
      <a href="/lapor">Laporkan Kehilangan</a>
    </div>
  </nav>
<div class="flex-center position-ref full-height">
    <div class="content">
    <img src="/assets/global/img/logo.png"></img>
    <form>
        <div id="custom-search-input">
            <div class="input-group col-md-12">
                <input type="text" class="  search-query form-control" placeholder="Search" />
                <span class="input-group-btn">
                <button class="btn btn-danger" type="button">
                <span class=" glyphicon glyphicon-search"></span>
                </button>
                </span>
            </div>
        </div>
    </form>
    <div class="panel panel-primary">
              <div class="panel-heading" style="background-color: #9d6617;">Jumlah Data</div>
              <table class="table table-bordered" style="color: #9d6617;">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Laporan</th>
                    <th>Kota</th>
                  </tr>
                </thead>
                <tbody>
                  <tr class="danger">
                    <td>1</td>
                    <td>Mark</td>
                    <td>Otto</td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Jacob</td>
                    <td>Red</td>
                  </tr>
                  <tr>
                    <td>3</td>
                    <td>the Bird</td>
                    <td>twitter</td>
                  </tr>
                </tbody>
              </table>
            </div>
    </div>
</div>
        
<footer class="footer">
    <span class="text-muted r-right">Copyright &copy; 2017 by <a href="mailto:rafinyadi@gmail.com">Rafi</a>.  All Rights Reserved.</span>
    <span class="text-muted"><a class="r-footext" href="/sk">Kebijakan Privasi</a>  <a class="r-footext" href="/tentang">Syarat Dan Ketentuan</a><a class="r-footext" href="/tentang">Tentang</a></span>
</footer>
    <!-- javascript -->
    <script src="/assets/global/js/jquery-3.1.1.min.js" type="text/javascript"></script>
    <script src="/assets/global/js/bootstrap.js" type="text/javascript"></script>
</body>
</html>
