    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="/css/style.css" rel="stylesheet" type="text/css">
    <link href="/assets/global/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="/assets/global/css/font-awesome.css" rel="stylesheet" type="text/css">