<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Cek Kendaraan</title>

@include('layouts.style')

@yield('css')

</head>
<body>
@include('layouts.header')

@yield('content')
        
@include('layouts.footer')

    <!-- javascript -->

@include('layouts.js')
@yield('js')

</body>
</html>
