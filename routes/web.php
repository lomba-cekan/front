<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => '/'], function () {
    Route::get('/','API\User\laporController@homeLapor');
    Route::get('/lapor','API\User\laporController@frontLapor');
    Route::post('/lapor/submit','API\User\laporController@addLapor');
Route::get('/lihat/{id}','API\User\laporController@showIdLapor');
    Route::get('/semua','API\User\laporController@showLapor');
    Route::post('/cari','API\User\laporController@searchLapor');
    Route::post('/melaporkan/submit','API\User\melaporkanController@melaporkan');
});

Route::group(['prefix' => 'adnmst4tor'], function () {
    Route::get('/home','API\Admin\laporController@home');
    Route::get('/','API\Admin\laporController@login');
    Route::get('/lihat/{id}','API\Admin\laporController@lihat');
    Route::get('/getuser','API\Admin\profileController@getProfile');
    Route::post('/verif','API\Admin\laporController@Verification');
    Route::get('/logout','API\Admin\authController@logout');
});

Route::group(['prefix' => 'auth'], function () {
    Route::post('/login','API\Admin\authController@login');
    Route::get('/','API\Admin\laporController@login');
});